﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public Task<Guid> CreateAsync(T data)
        {

            data.Id = data.Id == Guid.Empty ? Guid.NewGuid() : data.Id;
            Data = Data.Append(data);

            return Task.FromResult(data.Id);
        }

        public Task<Guid> UpdateAsync(T data)
        {
            Data = Data.Where(x => x.Id != data.Id);
            Data = Data.Append(data);

            return Task.FromResult(data.Id);
        }


        public Task<Guid> DeleteAsync(Guid id)
        {
            Data = Data.Where(x => x.Id != id);
            return Task.FromResult(id);
        }
    }
}