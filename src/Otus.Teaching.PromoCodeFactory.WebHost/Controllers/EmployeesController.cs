﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Classes;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }



        /// <summary>
        /// Создать данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateEmployeeAsync(EmployeeRequest employeeRequest)
        {
            try
            {
                if (employeeRequest == null)
                throw new NotFoundException("Employee not found");

            var guid = await _employeeRepository.CreateAsync(await MapEmployee(employeeRequest, null));

            return Ok(guid);
            }

            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }



        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployeeAsync(EmployeeRequestUpdate employeeRequest)
        {
            try
            {
                if (employeeRequest == null) throw new NotFoundException("Employee request not found");

                var employee = await _employeeRepository.GetByIdAsync(employeeRequest.Id);
                if (employee == null) throw new NotFoundException("Employee in not found");


                var resultUpdate = await _employeeRepository.UpdateAsync(await MapEmployee(employeeRequest, employee));

                return Ok(resultUpdate);
            }

            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
           
        }

        /// <summary>
        /// Удалить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    throw new NotFoundException("Employee id not found");

                var employee = await _employeeRepository.GetByIdAsync(id);

                if (employee == null)
                    throw new NotFoundException("Employee not found");

                var guid = _employeeRepository.DeleteAsync(employee.Id);

                return Ok(guid);

            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        private async Task<Employee> MapEmployee(IEmployeeRequest employeeRequest, Employee employee)
        {
            var roles = await CheckRolesAsync(employeeRequest.Roles);

            return new Employee()
            {
                Id = employee != null ? employee.Id : Guid.Empty,
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                Roles = roles.ToList()
            };
        }
        private async Task<IEnumerable<Role>> CheckRolesAsync(List<RoleItemRequest> roles)
        {

            if (!roles.Any())
                return null;

            var repositoryRoles = await _roleRepository.GetAllAsync();
            var rolesNotFound = roles.Where(a => !repositoryRoles.Any(b => a.Name.ToLower().Equals(b.Name.ToLower())));
            var rolesMapFound = repositoryRoles.Where(a => roles.Any(b => a.Name.ToLower().Contains(b.Name.ToLower())));

            if (rolesNotFound.Any())
                throw new NotFoundException($"Wrong role(s) for the employee: \"{String.Join(", ", rolesNotFound.Select(p => p.Name))}\"");


            return rolesMapFound;
        }
    }



}