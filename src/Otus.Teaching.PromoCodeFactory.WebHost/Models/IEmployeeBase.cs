﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public interface IEmployeeBase
    {
        public Guid Id { get; set; }
    }
}